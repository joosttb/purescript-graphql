{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name =
    "my-project"
, dependencies =
    [ "console"
    , "debug"
    , "effect"
    , "nullable"
    , "ordered-collections"
    , "prelude"
    , "psci-support"
    , "spec"
    , "typelevel"
    , "typelevel-prelude"
    , "variant"
    ]
, packages =
    ./packages.dhall
, sources =
    [ "src/**/*.purs" ]
}
