module Graph.Generic.Typed where

import Prelude

import Data.Newtype (class Newtype, unwrap)

newtype Typed node vars result = Typed node
--derive instance newtypeTyped :: Newtype ( Typed node vars result ) _
derive newtype instance eqTyped :: ( Eq node ) => Eq ( Typed node vars result )
derive newtype instance ordTyped :: ( Ord node ) => Ord ( Typed node vars result )

erase :: forall node vars result . Typed node vars result -> node
erase ( Typed node ) = node