module Graph.Format where

import Prelude

import Data.Foldable (intercalate)
import Data.FunctorWithIndex (mapWithIndex)
import Data.Maybe (Maybe(..), maybe)
import Data.Monoid (guard)
import Data.String as String
import Data.Variant (Variant, case_, on)
import Graph.Internal.Symbols as Sym
import Graph.Syntax as Graph

string :: Graph.Document -> String
string doc = ( mapWithIndex stringOperationNamed $ Graph.documentContent doc )
    # intercalate " "
        
stringOperationNamed :: Graph.Name -> Graph.OperationDefinition -> String
stringOperationNamed name op =
    show op.operation
    <> " " 
    <> Graph.nameValue name
    <> stringVariableDefinitions op.variables
    <> stringDirectives op.directives
    <> stringSelectionSet op.selection

stringOperation :: Graph.OperationDefinition -> String
stringOperation = stringOperationNamed ( Graph.unsafeName "" )


stringDirectives :: Graph.Directives -> String
stringDirectives [] = ""
stringDirectives directives =
    map stringDirective directives
    # intercalate ","

stringDirective :: Graph.Directive -> String
stringDirective ( Graph.Directive name arguments ) = "@" <> Graph.nameValue name <> stringArguments arguments 

stringVariableDefinitions :: Graph.VariableDefinitions -> String
stringVariableDefinitions [] = ""
stringVariableDefinitions defs =
    map stringVariableDefinition defs
    # intercalate ","
    # parameterify

stringVariableDefinition :: Graph.VariableDefinition -> String
stringVariableDefinition ( Graph.VariableDefinition name typeRef Nothing ) =
    ( Graph.nameValue name <> ":" <> stringTypeReference typeRef )
stringVariableDefinition ( Graph.VariableDefinition name typeRef ( Just value ) ) =
    ( Graph.nameValue name <> ":" <> stringTypeReference typeRef <> "=" <> stringConstValue value )

stringTypeReference :: Graph.TypeReference -> String
stringTypeReference ( Graph.TypeReference { nullable, typeReference } ) =
    let printRef = 
            case_
                # on Sym._listOf (\nestedRef -> stringTypeReference nestedRef # listify )
                # on Sym._named Graph.nameValue
    in printRef typeReference <> guard nullable "!"

stringEnumValue :: Graph.EnumValue -> String
stringEnumValue ( Graph.EnumValue v ) = v

stringVariable :: Graph.Variable -> String
stringVariable ( Graph.Variable v ) = "$" <> v

surround :: String -> String -> String -> String
surround start end content = start <> content <> end

listify :: String -> String
listify = surround "[" "]"

objectify :: String -> String
objectify = surround "{" "}"

parameterify :: String -> String
parameterify = surround "(" ")"

stringVariableValue :: ( Variant ( variable :: Graph.Variable ) ) -> String
stringVariableValue = case_
    # on Sym._variable stringVariable

stringList :: forall rest . ( Variant rest -> String ) -> Graph.List rest -> String
stringList rest ( Graph.List var ) =
    var
    # map ( stringBaseValue rest ) 
    # intercalate "," 
    # listify

stringObject :: forall rest . ( Variant rest -> String ) -> Graph.Object rest -> String
stringObject rest ( Graph.Object obj ) =
    let stringObjImpl name value = Graph.nameValue name <> ":" <> stringBaseValue rest value 
    in  mapWithIndex stringObjImpl obj
        # intercalate ","
        # objectify 

stringBaseValue :: forall rest . ( Variant rest -> String ) -> Graph.BaseValue rest -> String
stringBaseValue rest var =
    let match = rest
            # on Sym._int show
            # on Sym._float show
            # on Sym._string show
            # on Sym._null ( const "null" )
            # on Sym._boolean show
            # on Sym._enum stringEnumValue
            # on Sym._list ( stringList rest )
            # on Sym._object ( stringObject rest )
    in match var

stringValue :: Graph.Value -> String
stringValue v = stringBaseValue stringVariableValue v

stringConstValue :: Graph.ConstValue -> String
stringConstValue v = stringBaseValue case_ v

stringSelectionSet :: Graph.SelectionSet -> String
stringSelectionSet set =
    map stringSelection set
    # intercalate " "
    # objectify

stringSelection :: Graph.Selection -> String
stringSelection ( Graph.Field field ) =
    let alias = map Graph.nameValue field.alias
            # map ( _ <> ":" )
            # maybe "" identity

        set = map stringSelectionSet field.selection
            # maybe "" identity
    in alias <> Graph.nameValue field.name <> stringArguments field.arguments <> set
stringSelection ( Graph.InlineFragment frag ) =
    let typeCondition = map Graph.nameValue frag.onType
            # map ( "on " <> _ )
            # maybe "" identity
        directiveCondition = stringDirectives frag.directives
    in "... " <> typeCondition <> directiveCondition <> stringSelectionSet frag.selection
    
stringArguments :: Graph.Arguments -> String
stringArguments args = 
    let stringArgImpl name value = Graph.nameValue name <> ":" <> stringValue value
    in mapWithIndex stringArgImpl args
        # intercalate ","
        # \str -> guard ( String.length str > 0 ) $ parameterify str