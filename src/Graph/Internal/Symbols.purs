module Graph.Internal.Symbols where

import Data.Symbol (SProxy(..))

_operation = SProxy :: SProxy "operation"
_name = SProxy :: SProxy "name"
_variables = SProxy :: SProxy "variables"
_directives = SProxy :: SProxy "directives"
_selection = SProxy :: SProxy "selection"

_nullable = SProxy :: SProxy "nullable"
_typeReference = SProxy :: SProxy "typeReference"
_named = SProxy :: SProxy "named"
_listOf = SProxy :: SProxy "listOf"

_null = SProxy :: SProxy "null"
_int = SProxy :: SProxy "int" 
_string = SProxy :: SProxy "string"
_float = SProxy :: SProxy "float"
_boolean = SProxy :: SProxy "boolean"
_enum = SProxy :: SProxy "enum"
_list = SProxy :: SProxy "list"
_object = SProxy :: SProxy "object"
_variable = SProxy :: SProxy "variable"

_arguments = SProxy :: SProxy "arguments"
_onType = SProxy :: SProxy "onType"
_alias = SProxy :: SProxy "alias"

