module Graph.Generic where
  
import Prelude

import Data.Array.NonEmpty as NEArray
import Data.List (List)
import Data.Maybe (Maybe)
import Data.Nullable (Nullable)
import Data.Symbol (class IsSymbol, SProxy(..))
import Data.Symbol as Symbol
import Graph.Generic.Typed as Typed
import Graph.Syntax as Graph
import Prim.Row (class Cons)
import Prim.RowList (class RowToList, Cons, Nil)
import Type.Data.RowList (RLProxy(..))
import Type.Proxy (Proxy(..))

class GraphField field where
    fieldImpl :: Graph.Name -> Proxy field -> Graph.Selection 

field :: forall field
    . GraphField field
    => Graph.Name -> Proxy field -> Typed.Typed Graph.Selection {} field
field name prx = Typed.Typed $ fieldImpl name prx


instance fieldArray :: ( GraphField a ) => GraphField ( Array a ) where
    fieldImpl name _ = fieldImpl name ( Proxy :: Proxy a )
else
instance fieldList :: ( GraphField a ) => GraphField ( List a ) where
    fieldImpl name _ = fieldImpl name ( Proxy :: Proxy a )
else

instance fieldMaybe :: ( GraphField a ) => GraphField ( Maybe a ) where
    fieldImpl name _ = fieldImpl name ( Proxy :: Proxy a )
else
instance fieldNullable :: ( GraphField a ) => GraphField ( Nullable a ) where
    fieldImpl name _ = fieldImpl name ( Proxy :: Proxy a )
else

instance fieldRecord :: ( RowToList row rl, GraphSelection rl ) => GraphField ( Record row ) where
    fieldImpl name _ = 
        let set = Typed.erase $ selection ( Proxy :: Proxy ( Record row ) )
        in Graph.field name
            # Graph.selects set
else

instance fieldDefault :: GraphField a where
    fieldImpl name _ = Graph.field name


class GraphSelection rl where
    selectionImpl :: RLProxy rl -> Graph.SelectionSet

instance graphInferNil ::
    ( IsSymbol label
    , Cons label resultField () result
    , GraphField field
    ) => GraphSelection ( Cons label field Nil ) where
    selectionImpl _ =
        let
            name = Graph.unsafeName $ Symbol.reflectSymbol ( SProxy :: SProxy label )
            this = Typed.erase $ field name ( Proxy :: Proxy field ) 
        in NEArray.singleton $ this
else
instance graphInferCons ::
    ( IsSymbol label
    , GraphField field
    , GraphSelection right
    ) => GraphSelection ( Cons label field right ) where
    selectionImpl _ =
        let
            prev = selectionImpl ( RLProxy :: RLProxy right )
            name = Graph.unsafeName $ Symbol.reflectSymbol ( SProxy :: SProxy label )
            this = Typed.erase $ field name ( Proxy :: Proxy field )
        in NEArray.cons this prev

selection :: forall query rl
    . RowToList query rl
    => GraphSelection rl 
    => Proxy ( Record query ) -> Typed.Typed Graph.SelectionSet {} ( Record query )
selection _ = Typed.Typed $ selectionImpl ( RLProxy :: RLProxy rl )
