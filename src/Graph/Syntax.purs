module Graph.Syntax
    ( Document
    , Name
    , nameValue
    , unsafeName
    , name
    , Directive(..)
    , Directives
    , OperationDefinition
    , Arguments
    , documentContent
    , document
    , addOperation
    , OperationType(..)
    , TypeReference(..)
    , SelectionSet
    , Selection(..)
    , field
    , alias
    , selects
    , directives
    , Value
    , ConstValue
    , VariableDefinitions
    , VariableDefinition(..)
    , BaseValue(..)
    , Variable(..)
    , List(..)
    , Object(..)
    , EnumValue(..)
    ) where

import Prelude

import Data.Array.NonEmpty (NonEmptyArray)
import Data.Map as Dict
import Data.Maybe (Maybe(..))
import Data.String as String
import Data.Variant (Variant, inj)
import Graph.Internal.Symbols as Sym

newtype Name = Name String
derive newtype instance eqName :: Eq Name
derive newtype instance ordName :: Ord Name

name :: String -> Maybe Name
name str | String.length str > 0 = Just $ Name str
name str | otherwise = Nothing

unsafeName :: String -> Name
unsafeName = Name

nameValue :: Name -> String
nameValue ( Name n ) = n

data Directive = Directive Name Arguments
type Directives = Array Directive

newtype Document = Document ( Dict.Map Name OperationDefinition )
documentContent :: Document -> Dict.Map Name OperationDefinition 
documentContent ( Document map ) = map

document :: Name -> OperationDefinition -> Document
document opName op = Document $ Dict.singleton opName op

addOperation :: Name -> OperationDefinition -> Document -> Document
addOperation opName op ( Document map ) = Document $ Dict.insert opName op map

type OperationDefinition = 
    { operation :: OperationType
    , variables :: VariableDefinitions
    , directives :: Directives
    , selection :: SelectionSet
    }

data OperationType
    = Query
    | Mutation
    | Subscription
instance showOperationType :: Show OperationType where
    show Query = "query"
    show Mutation = "mutation"
    show Subscription = "subscription"

newtype TypeReference = TypeReference
    { nullable :: Boolean
    , typeReference :: Variant
        ( listOf :: TypeReference
        , named :: Name
        ) 
    }

typeName :: Name -> TypeReference
typeName tName = TypeReference
    { nullable : false
    , typeReference : inj Sym._named tName
    }

listOf :: TypeReference -> TypeReference
listOf ref = TypeReference 
    { nullable : false
    , typeReference : inj Sym._listOf $ ref
    }

nullable :: TypeReference -> TypeReference
nullable ( TypeReference ref ) = TypeReference ref { nullable = true }

newtype EnumValue = EnumValue String
newtype Variable = Variable String
newtype Object const = Object ( Dict.Map Name ( BaseValue const ) ) 
newtype List const = List ( Array ( BaseValue const ) )

type BaseValue rest = Variant
    ( int :: Int
    , float :: Number
    , string :: String
    , null :: Unit
    , boolean :: Boolean
    , enum :: EnumValue
    , list :: List rest
    , object :: Object rest
    | rest
    )

type ConstValue = BaseValue ()
type Value = BaseValue ( variable :: Variable )

data VariableDefinition 
    = VariableDefinition Name TypeReference ( Maybe ConstValue )
type VariableDefinitions = Array VariableDefinition

define :: Name -> TypeReference -> ConstValue -> VariableDefinition
define varName typeRef value = VariableDefinition varName typeRef ( Just value )

declare :: Name -> TypeReference -> VariableDefinition
declare varName typeRef = VariableDefinition varName typeRef Nothing

type SelectionSet = NonEmptyArray Selection
data Selection
    = Field
        { name :: Name
        , alias :: Maybe Name
        , arguments :: Arguments
        , selection :: Maybe SelectionSet
        , directives :: Directives
        }
    | InlineFragment
        { onType :: Maybe Name
        , directives :: Directives
        , selection :: SelectionSet
        }

field :: Name -> Selection
field fieldName =
    Field
        { name : fieldName
        , alias : Nothing
        , arguments : Dict.empty
        , directives : []
        , selection : Nothing
        }

typeFragment :: Name -> SelectionSet -> Selection
typeFragment tName sel =
    InlineFragment
        { onType : Just tName
        , directives : []
        , selection : sel 
        }

selects :: SelectionSet -> Selection -> Selection
selects set ( Field f ) = Field $ f { selection = Just set }
selects set ( InlineFragment f ) = InlineFragment $ f { selection = set }

alias :: Name -> Selection -> Selection
alias als ( Field f ) = Field $ f { alias = Just als }
alias _ f = f

directives :: Directives -> Selection -> Selection
directives dirs ( Field f ) = Field $ f { directives = dirs }
directives dirs ( InlineFragment f ) = InlineFragment $ f { directives = dirs }

type Arguments = Dict.Map Name Value

