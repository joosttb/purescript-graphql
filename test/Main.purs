module Test.Main where

import Prelude

import Effect (Effect)
import Effect.Aff (launchAff_)
import Graph.Format (stringBaseValue, stringValue)
import Graph.Variable (float, int, named)
import Graph.Variable as Var
import Test.Spec (describe, it)
import Test.Spec.Assertions (shouldEqual)
import Test.Spec.Reporter.Console (consoleReporter) as Reporter
import Test.Spec.Runner (runSpec)

main :: Effect Unit
main = launchAff_ $ runSpec [ Reporter.consoleReporter ] do
    describe "Graph.Format" do
        describe "values" do
          it "prints variable names" do
            shouldEqual ( stringValue $ Var.named "asdf" ) "$asdf"
          it "prints integer values" do
            shouldEqual ( stringValue $ Var.int 3 ) "3"
          it "prints float values" do
            shouldEqual ( stringValue $ Var.float 3.3 ) "3.3"
          it "prints string values" do
            shouldEqual ( stringValue $ Var.string "asdf" ) "\"asdf\""